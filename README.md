# EDM-study-path-recommender

## 1. Giới thiệu
Hệ thống gợi ý lộ trình học tập cho sinh viên khoa Công nghệ thông tin - trường Đại học Công Nghệ - Đại học Quốc Gia Hà Nội.

## 2. Các chức năng chính
Dựa vào khả năng, sở thích và định hướng của từng sinh viên, hệ thống đưa ra các chức năng chính sau :

(*Các chức năng chính đặt trong package [**main**](https://bitbucket.org/DuyenHa/edm-study-path-recommender/src/84c1f39270090c4500072459612d2ea658ec6fed/src/main/?at=master)* )
### 2.1 Gợi ý danh sách môn học
* **Mục tiêu** : gợi ý ra danh sách các môn học mà sinh viên nên học để hoàn thành chương trình học tương ứng.
* **INPUT**: 
.....

### 2.2 Gợi ý lộ trình môn học sắp xếp theo từng kì
* **Mục tiêu** : Dựa vào danh sách các môn học đã được gợi ý, hệ thống sẽ phân bổ các môn học này theo từng kì để sinh viên có hiệu quả học tập tốt nhất và hơn nữa là thỏa mãn các môn tiên quyết cần học trước.
* **INPUT** : ...

## 3. Các file đầu vào cần chuẩn bị
Các file cần chuẩn bị này được đặt trong cùng một folder ( như trong ví dụ ở mục [*newData/14020000*](https://bitbucket.org/DuyenHa/edm-study-path-recommender/src/84c1f39270090c4500072459612d2ea658ec6fed/newDATA/14020000/?at=master) )
### 3.1 Danh sách các môn đã học
* **Tên file** : learnt_courses.csv
* **Nội dung** : Danh sách các môn đã học của sinh viên
* **Định dạng** : mỗi dòng chứa các thông tin sau 
	* Mã sinh viên
	* Môn học này là tự chọn (1) hay là môn bắt buộc (0)
	* Mã môn học 

### 3.2 Độ đo lọc cộng tác
* **Tên file** : CF.csv
* **Nội dung** : Độ đo lọc cộng tác cho các môn **tự chọn** mà sinh viên **chưa học**
* **Định dạng** : mỗi dòng chứa các thông tin sau 
	* Mã sinh viên
	* Mã môn học 
	* Độ đo lọc cộng tác của môn đó.


### 3.3 Điểm dự đoán
* **Tên file** : predicted_score.csv
* **Nội dung** : Điểm dự đoán được cho các môn mà sinh viên **chưa học**
* **Định dạng** : mỗi dòng chứa các thông tin sau 
	* Mã sinh viên
	* Mã môn học 
	* Điểm dự đoán